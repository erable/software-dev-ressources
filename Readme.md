# Software development ressources

Compilation of useful resources for software development.

- `general_advices` directory : link to the contents of a sequence of small training sessions done in 2021 at LBMC (ENS Lyon) about software development practices in general.
  Keywords: git, testing, naming, refactoring, typing, optimisations.
- `rust_for_cpp_devs` : WIP talk to present rust to C++ devs
- `cplex` : notes on how to download *IBM Cplex*, and packaging code for debian-derived distributions
- `python_packaging_example` : (sad) notes about python packaging in 2022
