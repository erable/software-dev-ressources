from blah import functions
import sys

# main is isolated as a function for reuse : both in __main__.py and in generated "blah" binary
def main():
    if len(sys.argv) > 1 and sys.argv[1] == 'dsk':
        functions.dsk_help()
    else:
        functions.blah()

