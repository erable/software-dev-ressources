# __main__ has spacial handling : https://docs.python.org/3/library/__main__.html
# This is run when `python -m blah` is called.
import sys
from blah import main
sys.exit(main())
