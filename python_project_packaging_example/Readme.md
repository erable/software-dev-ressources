Python packaging example
========================

Example case : a python tool using a C++ binary called by subprocess.

Goal : package this tool *the Right Way* (tm), and be user-friendly.

Sadly the python packaging ecosystem seems fragmented and in transition.
There are multiple tools, and the documentation is fragmented between individual tools so it is difficult to determine what to use, when, and with what.
This documentation is an *okay* way to implement the example, to the best of my knowledge.

In Google, Python official documentation is often burried behind lots of advert-ridden and SEO-boosted websites.
Official documentation is sometimes difficult to find, and sometimes I just cannot find a useful official source at all.
I have linked some ressources relevant to the example when I could find them.

Package layout
--------------

Any non trivial script or library will grow to use multiple `.py` files.
**Python** itself defines a concept of modules and module hierarchy, with a specific file layout and import semantics : <https://docs.python.org/3/tutorial/modules.html#packages>.
This unofficial tutorial is the best I could find : <https://py-pkgs.org/04-package-structure#package-structure>.

### Application VS library
A python package is mostly made to represent a *library* − a collection of functions and classes exposed to other code.
For a pure library, it makes sense to add lots of metadata (version, context, API documentation) and to publish it to pip for wide diffusion.

An application is a code base which executable scripts, which mostly uses other libraries.
Due to python package structure, it will also appear as a library containing the app code.
A common pattern is to define a package with the code, including a `main()` function, and invoke it from multiple sources : `__main__.py`, *entry points*.
The current example is an application demonstrating all of those mecanisms.

### Imports
From <https://peps.python.org/pep-0008/#imports> : prefer absolute import.

Absolute import will only work when the package is installed.
Relative imports allow running the code from the source directory, but this has some caveats and is not recommended.
It is currently recommended to always install the package and run it as it would run on a user's machine, for reliability.
Absolute imports are also less ambiguous, especially in large projects with multiple directories, and provide better context in case of error.

### Layout
For the code, there are multiple recommended locations :
- `src/` layout : `<root>/src/<package-name>/__init__.py`
- *flat* layout : `<root>/<package-name>/__init__.py`

Many build-system backends support both, with various defaults. This is a stylistic choice so feel free to choose.
`setuptools` defaults to flat layout, Poetry defaults to src layout.

### Tests
I used to put unittests inside source code, and run them with `python -m unittest <package-module-spec>`.
I liked using the tests as *usage documentation* close to the code itself, and being able to test internal helpers.
This is not recommended for multiple reasons :
- waste of ressources : test code is shipped and parsed on each import, while being unused
- `unittest` test discovery is finicky to use
- test code may rely on the package being installed properly beforehand

The current recommended way is to put them in a separate `tests/` subdir, and use the test tooling of your choice : <https://py-pkgs.org/05-testing#test-structure>.

Testing tooling :
- `unittest` is a standard package, but lacks some ergonomics for test discoverability and test runners
- there are multiple alternatives ; `tox` is often mentionned in posts, but I have not tried it yet.

Build system rationale
----------------------

The role of a build-system is to take the package files, and build an archive containing them in a format suitable for the target :
- sdist : "source distribution"
- wheel : "binary" format, used by pip
- egg : deprecated binary format

There are many build-systems *backends* :
- setuptools : old, but still widely used and supported, and considered the default by the Debian linux distribution. Has a lot of features including : binary extension (.so), application script generation, command override / custom.
- poetry: more "modern", seems more integrated. C extension not mentionned in documentation, stackoverflow mentions using custom build script with distutils call. Supports script install with custom tags at least.
- flit: advertised as poetry but smaller and simpler.

For years setuptools was the *de-facto* build-system for python, with the ubuquitous `setup.py` file that is now being slowly phased out (for security reasons).
PEP517 (<https://peps.python.org/pep-0517/>) was introduced to finally create a standard *build declaration* for python, which is independent on the actual build-system backend.
This declaration is made using `pyproject.toml`, which includes general metadata (package name, version, dependencies), and a choice of a build backend with its backend-specific configuration.

You should look at your specific backend documentation to know what to write :
- setuptools : <https://setuptools.pypa.io/en/latest/userguide/quickstart.html>
- poetry : the nice tutorial is based on it : <https://py-pkgs.org/03-how-to-package-a-python>

This shared format is then used by various tools to actually perform the build :
- `pip install .` : package and install the current project
- `python -m build` : another generic frontend to build a python package, built as an example for the PEP517.

Setuptools
----------

Setuptools is a successor of distutils, and exists since at least a decade.
Is it still the default system for build automation in debian packaging : <https://wiki.debian.org/Python/LibraryStyleGuide>.
And it has a decade of stackoverflow examples for many advanced features (binary, FFI, etc) to rely upon.
For these reasons setuptools is used here.

As of 09/2022, the compatibility of setuptools with `pyproject.toml` (PEP517) is still recent, so be sure to update setuptools or create a fresh conda/virtualenv in case of problems.

### Useful features
- entry points / scripts : create a binary file calling a function in the module : <https://setuptools.pypa.io/en/latest/userguide/entry_point.html>
- FFI with C modules : not used here. Pybind11 is nice to interface with modern C++, PyO3 for Rust.
- command customization : interesting but not recommended as calling setup.py manually is deprecated ; <https://setuptools.pypa.io/en/latest/deprecated/commands.html>.

### Example with custom C++ binary alongside python code
An initial idea was to customize the `setup.py install` command to also install the binary from source.
However this would recompile the binary on every install, which is wasteful and annoying.

A better solution is to provide a script that installs from source, which a user can call once if the binary tool is not installed.
This script is better standalone ; there is no advantage to integrate it within `setup.py`.
The script uses <https://stackoverflow.com/questions/33168482/compiling-installing-c-executable-using-pythons-setuptools-setup-py> as inspiration, especially for the virtualenv prefix.

Playing with the example
------------------------

```bash
# Create a virtualenv to not trash your current python environment.
# If you want to actually install the package, just skip these lines.
# But be sure to have the correct environment activated beforehand (conda, venv, ...).
python -m venv .env # create env files, can be removed safely to delete it
source .env/bin/activate # setup bash variables to use the env (PATH, PYTHON_PATH, etc)

# Install package and its dependencies, when run in the directory of the pyproject.toml
pip install .

# Now the 'blah' package can be used
python -m blah # module exec, __main__.py
blah # entry point script defined in pyproject.toml
python -c "import blah.functions; blah.functions.blah()" # access as library

# blah package needs GATB/dsk binary to "run" ; it must be installed in PATH.
# install_dsk.py will install GATB/dsk from source in the current python PATH (virtualenv/conda if used)
python install_dsk.py
# The script also support installing to the "user" location like `pip install --user`
# Warning : this ignores the virtualenv configuration !
python install_dsk.py --user

# Running blah with dsk "functionnality"
blah dsk # prints dsk help

# If using the virtualenv and the install is not needed anymore,
# cleanup can be done by simply deleting the directory containing the installed files.
rm -rf .env/
```

Always be careful with `pip`, as it will download and install dependencies without confirmation.
This may run arbitrary install scripts, which has already been used to install malware (*supply chain attack*).
In addition, there is no guarantee that packages will be uninstalled properly.
`pip uninstall <package>` will only remove the package but not its dependencies.

