# This file is only a stub for backward-compatibility ; it will pull its configuration from the modern pyproject.toml.
# This is documented at https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
from setuptools import setup
setup()
