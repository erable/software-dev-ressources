# Download and install dsk (C++ binary) from source.
# If run within a virtualenv / conda, this should install the binary to the local bin/ directory.
# Takes inspiration from https://stackoverflow.com/questions/33168482/compiling-installing-c-executable-using-pythons-setuptools-setup-py
import tempfile
import subprocess
import os
import sys
import site

# Support user-wide install like `pip install --user` (`setup.py install --user`)
if len(sys.argv) > 1 and sys.argv[1] == '--user':
    prefix = site.USER_BASE
else:
    prefix = sys.prefix

with tempfile.TemporaryDirectory('python-dsk') as directory:
    def run(args, cwd):
        print(' '.join(args))
        subprocess.run(args, cwd = cwd, check = True)
    run(['git', 'clone', '--recursive', 'https://github.com/GATB/dsk.git'], cwd = directory)
    # Cloned into dsk/ subdir
    git = os.path.join(directory, 'dsk') 
    # using sys.prefix to determine location of python's /bin. This may be a virtualenv or /usr.
    # Always override cmake which would default to /usr/local if not specified.
    run(['cmake', '.', f"-DCMAKE_INSTALL_PREFIX={prefix}"], cwd = git)
    run(['make', '-j4'], cwd = git)
    # GATB tools usually have a broken install target, just extract the binary
    # On tools with a sane buildsystem you could just call make install, which would use the prefix
    run(['cp', 'bin/dsk', f"{prefix}/bin/dsk"], cwd = git)
